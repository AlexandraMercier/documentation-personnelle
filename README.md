# Documentation Personnelle

Choose a self-explaining name for your project.

## État du projet

En ce moment, je le fais principalement pour archiver mes connaissances, et développer un workflow avec Sphinx.

## Description

Une archive de Gitlab Pages de mes connaissances cumulées. C'est aussi une opportunitée pour moi d'apprendre [Sphinx](https://www.sphinx-doc.org/en/master/)

## Licence

Vous pouvez faire ce que vous voulez, tant que je suis citée :)
